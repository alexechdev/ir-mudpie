from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential, Model 
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras import backend as k 
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, EarlyStopping
import coremltools

img_width, img_height = 256, 256
train_data_dir = "dataset/train"
validation_data_dir = "dataset/test"
nb_train_samples = 1000
nb_validation_samples = 100 
batch_size = 20
epochs = 1

model = ResNet50(include_top=False, weights='imagenet', input_tensor=None, input_shape=(img_width, img_height, 3), pooling=None, classes=1000)

print("[INFO]: Init retrain model")
# Freeze the layers which you don't want to train. Here I am freezing the all layers.
for layer in model.layers[:]:
    layer.trainable = False

# Adding custom Layer
# We only add
x = model.output
x = GlobalAveragePooling2D()(x)
# x = Flatten()(x)
# Adding even more custom layers
x = Dense(1024, activation="relu")(x)
# x = Dropout(0.5)(x)
x = Dense(1024, activation="relu")(x)
x = Dense(512, activation="relu")(x)
print("[INFO]: Runing predictions")
predictions = Dense(1, activation="softmax")(x)

print("[INFO]: Creating model")
# creating the final model 
model_final = Model(inputs = model.input, outputs = predictions)

print("[INFO]: Init training...")
# Initiate the train and test generators with data Augumentation 
train_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
test_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)

train_generator = train_datagen.flow_from_directory(
  train_data_dir,
  target_size = (img_height, img_width),
  batch_size = batch_size,
  class_mode = "categorical")

validation_generator = test_datagen.flow_from_directory(
  validation_data_dir,
  target_size = (img_height, img_width),
  class_mode = "categorical")

print("[INFO]: Compiling model")
# compile the model 
model_final.compile(optimizer="Adam", loss = "binary_crossentropy", metrics=["accuracy"])

# Save the model according to the conditions  
checkpoint = ModelCheckpoint("model-resnet50-final.h5", monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
early = EarlyStopping(monitor='val_acc', patience=10)

step_size = train_generator.n//train_generator.batch_size
# Train the model 
model_final.fit_generator(
  train_generator,
  steps_per_epoch =  step_size,
  epochs = epochs)
  #validation_data = validation_generator,
  #validation_steps = nb_validation_samples,
  #callbacks = [checkpoint, early])
print("[INFO]: Finish training")

# labels = model_final.predict(x)
# classes = labels.argmax(axis=1)

print("[INFO]: Exporting...")
coreml_model = coremltools.converters.keras.convert(
    model_final, 
    input_names = ['image'],
    output_names = ['output'],
    class_labels = ['pets'], 
    image_input_names = 'image')

# Saving the Core ML model to a file.
coreml_model.save('Mudpie.mlmodel')
print("[INFO]: Finish exporting saved as Mudpie.mlmodel")
